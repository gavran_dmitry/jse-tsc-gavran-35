package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.api.repository.ISessionRepository;
import ru.tsc.gavran.tm.model.Session;

public class SessionRepository extends AbstractOwnerRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }
}
