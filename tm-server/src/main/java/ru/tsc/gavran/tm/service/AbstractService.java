package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.empty.EmptyIndexException;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    @NotNull
    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Comparator<E> comparator) {
        if (comparator == null) return null;
        return repository.findAll(comparator);
    }

    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        repository.add(entity);
        return entity;
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(index);
    }

    @NotNull
    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public void addAll(@Nullable final List<E> entities) {
        if (entities == null) return;
        repository.addAll(entities);
    }

}