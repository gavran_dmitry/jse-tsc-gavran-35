package ru.tsc.gavran.tm.exception.entity;

import ru.tsc.gavran.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
